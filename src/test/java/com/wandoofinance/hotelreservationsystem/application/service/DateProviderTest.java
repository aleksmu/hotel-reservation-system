package com.wandoofinance.hotelreservationsystem.application.service;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class DateProviderTest {
    private DateProvider dateProvider = new DateProvider();

    @Test
    public void shouldBeAbleToSetDate() {
        LocalDate customDate = LocalDate.of(2018, 8, 19);

        dateProvider.setCurrentDate(customDate);

       assertEquals(customDate,dateProvider.today());
    }

    @Test
    public void shouldReturnCurrentDate() {
        LocalDate today = dateProvider.today();

        assertEquals(LocalDate.now(), today);

    }
}