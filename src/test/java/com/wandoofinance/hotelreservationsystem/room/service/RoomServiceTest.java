package com.wandoofinance.hotelreservationsystem.room.service;

import com.wandoofinance.hotelreservationsystem.application.service.DateProvider;
import com.wandoofinance.hotelreservationsystem.reservation.entity.Reservation;
import com.wandoofinance.hotelreservationsystem.reservation.service.ReservationService;
import com.wandoofinance.hotelreservationsystem.room.dto.RoomDTO;
import com.wandoofinance.hotelreservationsystem.room.entity.Room;
import com.wandoofinance.hotelreservationsystem.room.repository.RoomRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomServiceTest {
    @Autowired
    private RoomService roomService;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private DateProvider dateProviderMock = Mockito.mock(DateProvider.class);

    private RoomDTO roomDTO = new RoomDTO();

    @Test
    public void shouldAddRoomToDatabase() {
        roomDTO.setFloor(1);
        roomDTO.setDoubleBed(true);
        roomDTO.setHaveWiFi(true);

        Room room = roomService.addRoomToDatabase(roomDTO);

        assertEquals(room.getId(), roomRepository.findOne(room.getId()).getId());
        assertEquals(room.getFloor(), roomRepository.findOne(room.getId()).getFloor());
        assertEquals(room.isHaveWiFi(), roomRepository.findOne(room.getId()).isHaveWiFi());
        assertEquals(room.isDoubleBed(), roomRepository.findOne(room.getId()).isDoubleBed());
    }

    @Test
    public void shouldBeAbleToViewRoom() {
        roomDTO.setFloor(1);
        roomDTO.setDoubleBed(true);
        roomDTO.setHaveWiFi(true);
        Room room = roomService.addRoomToDatabase(roomDTO);

        Room room2 = roomService.viewRoom(room.getId());

        assertEquals(room.getId(), room2.getId());
        assertEquals(room.getFloor(), room2.getFloor());
        assertEquals(room.isHaveWiFi(), room2.isHaveWiFi());
        assertEquals(room.isDoubleBed(), room2.isDoubleBed());
    }

    @Test
    public void shouldBeAbleToViewAllRooms() {
        roomRepository.deleteAll();
        roomService.addRoomToDatabase(roomDTO);
        roomService.addRoomToDatabase(roomDTO);

        Iterable<Room> viewRooms = roomService.viewAllRooms();
        List<Room> actualRoomList = new ArrayList<>();
        viewRooms.forEach(actualRoomList::add);

         assertEquals(2,actualRoomList.size());
    }

    @Test
    public void shouldBeAbleToDeleteRoom() {
        roomRepository.deleteAll();
        Room room = roomService.addRoomToDatabase(roomDTO);
        roomService.deleteRoom(room.getId());

        assertEquals(0, roomRepository.count());
    }

    @Test
    public void shouldBeAbleToEditRoom() {
        roomDTO.setFloor(1);
        roomDTO.setDoubleBed(true);
        roomDTO.setHaveWiFi(true);
        RoomDTO roomDTO2 = new RoomDTO();
        roomDTO2.setFloor(3);
        roomDTO2.setHaveWiFi(false);
        roomDTO2.setDoubleBed(false);

        Room room = roomService.addRoomToDatabase(roomDTO);
        Room room2 = roomService.editRoom(room.getId(), roomDTO2);

        assertEquals(3, (int) room2.getFloor());
        assertFalse(room2.isHaveWiFi());
        assertFalse(room2.isDoubleBed());
    }

    @Test
    public void shouldShowCorrectStatisticsIfOneRoomAvailableAndOneOccupied() {
        roomRepository.deleteAll();
        LocalDate startDate = LocalDate.of(2019, 6, 26);
        LocalDate endDate = LocalDate.of(2019, 6, 30);

        Room room = roomService.addRoomToDatabase(roomDTO);
        roomService.addRoomToDatabase(roomDTO);
        reservationService.roomReservation(room.getId(), startDate, endDate);

        assertEquals(1, (int) roomService.showStatistics(startDate, endDate).getAvailableRoomCount());
        assertEquals(1, (int) roomService.showStatistics(startDate, endDate).getOccupiedRoomCount());
    }

    @Test
    public void shouldShowCorrectStatisticsIfBothRoomsAvailable() {
        roomRepository.deleteAll();
        LocalDate startDate = LocalDate.of(2019, 6, 26);
        LocalDate endDate = LocalDate.of(2019, 6, 30);

        roomService.addRoomToDatabase(roomDTO);
        roomService.addRoomToDatabase(roomDTO);

        assertEquals(2, (int) roomService.showStatistics(startDate, endDate).getAvailableRoomCount());
        assertEquals(0, (int) roomService.showStatistics(startDate, endDate).getOccupiedRoomCount());

    }

    @Test
    public void shouldShowCorrectStatisticsIfBothRoomsOccupied() {
        roomRepository.deleteAll();
        LocalDate startDate = LocalDate.of(2019, 6, 26);
        LocalDate endDate = LocalDate.of(2019, 6, 30);

        Room room = roomService.addRoomToDatabase(roomDTO);
        Room room2 = roomService.addRoomToDatabase(roomDTO);
        reservationService.roomReservation(room.getId(), startDate, endDate);
        reservationService.roomReservation(room2.getId(), startDate, endDate);


        assertEquals(0, (int) roomService.showStatistics(startDate, endDate).getAvailableRoomCount());
        assertEquals(2, (int) roomService.showStatistics(startDate, endDate).getOccupiedRoomCount());
    }

    @Test
    public void shouldShowCorrectScheduleListForRoom() {
        roomRepository.deleteAll();
        LocalDate startDate = LocalDate.of(2019, 6, 26);
        LocalDate endDate = LocalDate.of(2019, 6, 30);
        Room room = roomService.addRoomToDatabase(roomDTO);
        reservationService.roomReservation(room.getId(), startDate, endDate);

        Iterable<Reservation> reservationList = roomService.showScheduleForRoom(room.getId());
        List<Reservation> actualReservationList = new ArrayList<>();
        reservationList.forEach(actualReservationList::add);

        assertEquals(1,actualReservationList.size());
    }

    @Test
    public void scheduleListForRoomShouldBeEmptyIfNoReservationsInFuture() {
        dateProviderMock.setCurrentDate(LocalDate.of(2018,5,30));
        LocalDate startDate = LocalDate.of(2018, 6, 26);
        LocalDate endDate = LocalDate.of(2018, 6, 30);
        Room room = roomService.addRoomToDatabase(roomDTO);
        reservationService.roomReservation(room.getId(), startDate, endDate);

        dateProviderMock.setCurrentDate(LocalDate.now());
        Iterable<Reservation> reservationList = roomService.showScheduleForRoom(room.getId());
        List<Reservation> actualReservationList = new ArrayList<>();
        reservationList.forEach(actualReservationList::add);

        assertTrue(actualReservationList.isEmpty());
    }

    @Test
    public void shouldShowCorrectScheduleListIfStartDatePassedButEndDateValid() {
        dateProviderMock.setCurrentDate(LocalDate.of(2019, 4, 25));
        LocalDate startDate = LocalDate.of(2019, 4, 26);
        LocalDate endDate = LocalDate.of(2019, 8, 30);
        Room room = roomService.addRoomToDatabase(roomDTO);
        reservationService.roomReservation(room.getId(), startDate, endDate);

        dateProviderMock.setCurrentDate(LocalDate.of(2019, 4, 29));
        Iterable<Reservation> reservationList = roomService.showScheduleForRoom(room.getId());
        List<Reservation> actualReservationList = new ArrayList<>();
        reservationList.forEach(actualReservationList::add);

        assertEquals(1,actualReservationList.size());
    }
}