package com.wandoofinance.hotelreservationsystem.room.mapper;

import com.wandoofinance.hotelreservationsystem.room.dto.RoomDTO;
import com.wandoofinance.hotelreservationsystem.room.entity.Room;
import com.wandoofinance.hotelreservationsystem.room.mapper.RoomMapper;
import org.junit.Test;

import static org.junit.Assert.*;

public class RoomMapperTest {
    private RoomDTO roomDTO = new RoomDTO();
    private Room room = new Room();
    private RoomMapper roomMapper = new RoomMapper();

    @Test
    public void shouldMapRoomDTOtoEntity() {
        roomDTO.setFloor(1);
        roomDTO.setHaveWiFi(true);
        roomDTO.setDoubleBed(false);

        room = roomMapper.roomDTOtoEntity(roomDTO);

        assertEquals(1, (int) room.getFloor());
        assertTrue(room.isHaveWiFi());
        assertFalse(room.isDoubleBed());
    }

    @Test
    public void shouldMapRoomEntityToRoomDTO() {
        room.setFloor(3);
        room.setHaveWiFi(false);
        room.setDoubleBed(true);

        roomDTO = roomMapper.roomEntityToRoomDTO(room);

        assertEquals(3,(int) roomDTO.getFloor());
        assertFalse(roomDTO.isHaveWiFi());
        assertTrue(roomDTO.isDoubleBed());
    }
}