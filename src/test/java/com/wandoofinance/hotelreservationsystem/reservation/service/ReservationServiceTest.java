package com.wandoofinance.hotelreservationsystem.reservation.service;

import com.wandoofinance.hotelreservationsystem.reservation.dto.ReservationStatus;
import com.wandoofinance.hotelreservationsystem.reservation.dto.ReservationStatusDTO;
import com.wandoofinance.hotelreservationsystem.reservation.entity.Reservation;
import com.wandoofinance.hotelreservationsystem.room.dto.RoomDTO;
import com.wandoofinance.hotelreservationsystem.room.entity.Room;
import com.wandoofinance.hotelreservationsystem.room.repository.RoomRepository;
import com.wandoofinance.hotelreservationsystem.room.service.RoomService;
import org.hibernate.Hibernate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReservationServiceTest {
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private RoomRepository roomRepository;
    private RoomDTO roomDTO = new RoomDTO();
    private ReservationStatusDTO reservationStatusDTO = new ReservationStatusDTO();

    @Test
    public void shouldReturnSuccessfulStatusIfValidReservation() {
        Room room = roomService.addRoomToDatabase(roomDTO);
        LocalDate startDate = LocalDate.of(2020, 5, 20);
        LocalDate endDate = LocalDate.of(2020, 5, 22);

        reservationStatusDTO = reservationService.roomReservation(room.getId(), startDate, endDate);

        assertEquals(ReservationStatus.SUCCESSFUL, reservationStatusDTO.getStatus());
    }

    @Test
    public void shouldReturnFailedStatusIfInvalidReservation() {
        Room room = roomService.addRoomToDatabase(roomDTO);
        LocalDate startDate = LocalDate.of(2020, 5, 20);
        LocalDate endDate = LocalDate.of(2020, 5, 22);

        reservationService.roomReservation(room.getId(), startDate, endDate);
        reservationStatusDTO = reservationService
                .roomReservation(room.getId(), startDate, endDate);

        assertEquals(ReservationStatus.FAILED,reservationStatusDTO.getStatus());
    }

    @Test
    public void shouldIterateAvailableRooms() {
        roomRepository.deleteAll();
        roomService.addRoomToDatabase(roomDTO);
        roomService.addRoomToDatabase(roomDTO);
        LocalDate startDate = LocalDate.of(2020, 5, 20);
        LocalDate endDate = LocalDate.of(2020, 5, 22);

        Iterable<RoomDTO> reservationIterable = reservationService.checkAvailability(startDate,endDate);
        List<RoomDTO> actualRoomList = new ArrayList<>();
        reservationIterable.forEach(actualRoomList::add);

        assertEquals(2,actualRoomList.size());
    }

    @Test
    public void availableRoomsShouldBeEmptyIfNoRoomsAvailable(){
        roomRepository.deleteAll();
        Room room = roomService.addRoomToDatabase(roomDTO);
        LocalDate startDate = LocalDate.of(2020, 5, 20);
        LocalDate endDate = LocalDate.of(2020, 5, 22);
        reservationService.roomReservation(room.getId(),startDate,endDate);

        Iterable<RoomDTO> reservationIterable = reservationService.checkAvailability(startDate,endDate);

        assertFalse(reservationIterable.iterator().hasNext());
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionIfInvalidDateProvidedForReservation(){
        Room room = roomService.addRoomToDatabase(roomDTO);
        LocalDate startDate = LocalDate.of(2019, 5, 25);
        LocalDate endDate = LocalDate.of(2019, 5, 22);

        reservationService.roomReservation(room.getId(), startDate, endDate);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionIfInvalidIdDuringReservation() {
        LocalDate startDate = LocalDate.of(2020, 5, 20);
        LocalDate endDate = LocalDate.of(2020, 5, 22);

        reservationStatusDTO = reservationService.roomReservation(99L, startDate, endDate);
    }

}