package com.wandoofinance.hotelreservationsystem.reservation.mapper;

import com.wandoofinance.hotelreservationsystem.reservation.entity.Reservation;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class ReservationMapperTest {
    private ReservationMapper reservationMapper = new ReservationMapper();

    @Test
    public void reservationParamToEntity() {
        LocalDate startDate = LocalDate.of(2019,8,8);
        LocalDate endDate = LocalDate.of(2019,8,10);

        Reservation reservation = reservationMapper.reservationParamToEntity(startDate, endDate);

        assertEquals(LocalDate.of(2019,8,8), reservation.getStartDate());
        assertEquals(LocalDate.of(2019,8,10), reservation.getEndDate());
    }
}