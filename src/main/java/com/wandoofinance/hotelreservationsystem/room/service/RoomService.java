package com.wandoofinance.hotelreservationsystem.room.service;

import com.wandoofinance.hotelreservationsystem.application.dto.StatisticsDTO;
import com.wandoofinance.hotelreservationsystem.application.service.DateProvider;
import com.wandoofinance.hotelreservationsystem.reservation.entity.Reservation;
import com.wandoofinance.hotelreservationsystem.room.dto.RoomDTO;
import com.wandoofinance.hotelreservationsystem.room.entity.Room;
import com.wandoofinance.hotelreservationsystem.room.mapper.RoomMapper;
import com.wandoofinance.hotelreservationsystem.room.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class RoomService {
    private RoomRepository roomRepository;
    private RoomMapper roomMapper;
    private DateProvider dateProvider;

    @Autowired
    public RoomService(RoomRepository roomRepository, RoomMapper roomMapper, DateProvider dateProvider) {
        this.roomRepository = roomRepository;
        this.roomMapper = roomMapper;
        this.dateProvider = dateProvider;
    }

    @Transactional
    public Room addRoomToDatabase(RoomDTO roomDTO) {
        Room room = roomMapper.roomDTOtoEntity(roomDTO);
        roomRepository.save(room);
        return room;
    }

    public Room viewRoom(Long id) {
        return roomRepository.findOne(id);
    }

    public Iterable<Room> viewAllRooms() {
        return roomRepository.findAll();
    }

    public void deleteRoom(Long id) {
        roomRepository.delete(id);
    }

    @Transactional
    public Room editRoom(Long id, RoomDTO roomDTO) {
        Room room = roomRepository.findOne(id);
        room.setFloor(roomDTO.getFloor());
        room.setHaveWiFi(roomDTO.isHaveWiFi());
        room.setDoubleBed(roomDTO.isDoubleBed());
        roomRepository.save(room);
        return room;
    }

    @Transactional
    public StatisticsDTO showStatistics(LocalDate startDate, LocalDate endDate) {
        dateCheck(startDate, endDate);
        Iterable<Room> roomIterable = roomRepository.findAll();
        List<Room> roomList = new ArrayList<>();
        roomIterable.forEach(roomList::add);
        return checkRoomReservations(roomList, startDate, endDate);
    }

    private StatisticsDTO checkRoomReservations(List<Room> roomList, LocalDate startDate, LocalDate endDate) {
        Integer occupiedRoomCount = 0;
        Integer availableRoomCount = 0;
        StatisticsDTO statisticsDTO = new StatisticsDTO();
        for (Room viewRoom : roomList) {
            if(isRoomAvailable(viewRoom.getReservations(), startDate, endDate)
                    || viewRoom.getReservations().isEmpty()){
                availableRoomCount++;
            } else {
                occupiedRoomCount++;
            }
        }
        statisticsDTO.setOccupiedRoomCount(occupiedRoomCount);
        statisticsDTO.setAvailableRoomCount(availableRoomCount);
        return statisticsDTO;
    }

    private boolean isRoomAvailable(List<Reservation> reservationList, LocalDate startDate,
                                    LocalDate endDate) {
        boolean isRoomAvailable = true;
        for (Reservation viewReservation : reservationList) {
            if (startDate.isBefore(viewReservation.getEndDate())
                    && viewReservation.getStartDate().isBefore(endDate)) {
                isRoomAvailable = false;
            }
        }
        return isRoomAvailable;
    }

    @Transactional
    public Iterable<Reservation> showScheduleForRoom(Long id) {
        List<Reservation> reservationList = new ArrayList<>();
        Room room = roomRepository.findOne(id);
        for (Reservation viewRoom : room.getReservations()) {
            if (viewRoom.getEndDate().isAfter(dateProvider.today())) {
                reservationList.add(viewRoom);
            }
        }
        return reservationList;
    }

    private void dateCheck(LocalDate startDate, LocalDate endDate) {
        if (startDate.isBefore(dateProvider.today()) || endDate.isBefore(startDate)) {
            throw new IllegalStateException();
        }
    }

}
