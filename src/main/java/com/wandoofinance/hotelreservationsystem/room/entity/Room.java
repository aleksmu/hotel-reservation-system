package com.wandoofinance.hotelreservationsystem.room.entity;

import com.wandoofinance.hotelreservationsystem.reservation.entity.Reservation;

import javax.persistence.*;
import java.util.List;

@Entity
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer floor;

    private boolean haveWiFi;

    private boolean doubleBed;

    @OneToMany
    private List<Reservation> reservations;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public boolean isHaveWiFi() {
        return haveWiFi;
    }

    public void setHaveWiFi(boolean haveWiFi) {
        this.haveWiFi = haveWiFi;
    }

    public boolean isDoubleBed() {
        return doubleBed;
    }

    public void setDoubleBed(boolean doubleBed) {
        this.doubleBed = doubleBed;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
