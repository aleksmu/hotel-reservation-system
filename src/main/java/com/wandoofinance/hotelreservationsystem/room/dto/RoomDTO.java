package com.wandoofinance.hotelreservationsystem.room.dto;

public class RoomDTO {

    private Long id;

    private Integer floor;

    private boolean haveWiFi;

    private boolean doubleBed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public boolean isHaveWiFi() {
        return haveWiFi;
    }

    public void setHaveWiFi(boolean haveWiFi) {
        this.haveWiFi = haveWiFi;
    }

    public boolean isDoubleBed() {
        return doubleBed;
    }

    public void setDoubleBed(boolean doubleBed) {
        this.doubleBed = doubleBed;
    }
}
