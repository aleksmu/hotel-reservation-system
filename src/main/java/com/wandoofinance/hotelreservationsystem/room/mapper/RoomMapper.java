package com.wandoofinance.hotelreservationsystem.room.mapper;

import com.wandoofinance.hotelreservationsystem.room.dto.RoomDTO;
import com.wandoofinance.hotelreservationsystem.room.entity.Room;
import org.springframework.stereotype.Component;

@Component
public class RoomMapper {

    public Room roomDTOtoEntity(RoomDTO roomDTO){
        Room room = new Room();
        room.setFloor(roomDTO.getFloor());
        room.setHaveWiFi(roomDTO.isHaveWiFi());
        room.setDoubleBed(roomDTO.isDoubleBed());
        return room;
    }

    public RoomDTO roomEntityToRoomDTO(Room room){
        RoomDTO roomDTO = new RoomDTO();
        roomDTO.setId(room.getId());
        roomDTO.setFloor(room.getFloor());
        roomDTO.setHaveWiFi(room.isHaveWiFi());
        roomDTO.setDoubleBed(room.isDoubleBed());
        return roomDTO;
    }

}
