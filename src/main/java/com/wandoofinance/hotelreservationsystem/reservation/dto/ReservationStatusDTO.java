package com.wandoofinance.hotelreservationsystem.reservation.dto;

public class ReservationStatusDTO {
    private ReservationStatus status;

    private String statusDetails;

    public String getStatusDetails() {
        return statusDetails;
    }

    public void setStatusDetails(String statusDetails) {
        this.statusDetails = statusDetails;
    }

    public ReservationStatus getStatus() {
        return status;
    }

    public void setStatus(ReservationStatus status) {
        this.status = status;
    }
}
