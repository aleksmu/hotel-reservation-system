package com.wandoofinance.hotelreservationsystem.reservation.service;


import com.wandoofinance.hotelreservationsystem.application.service.DateProvider;
import com.wandoofinance.hotelreservationsystem.reservation.dto.ReservationStatus;
import com.wandoofinance.hotelreservationsystem.reservation.dto.ReservationStatusDTO;
import com.wandoofinance.hotelreservationsystem.reservation.entity.Reservation;
import com.wandoofinance.hotelreservationsystem.reservation.mapper.ReservationMapper;
import com.wandoofinance.hotelreservationsystem.reservation.repository.ReservationRepository;
import com.wandoofinance.hotelreservationsystem.room.dto.RoomDTO;
import com.wandoofinance.hotelreservationsystem.room.entity.Room;
import com.wandoofinance.hotelreservationsystem.room.mapper.RoomMapper;
import com.wandoofinance.hotelreservationsystem.room.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
public class ReservationService {
    private RoomRepository roomRepository;
    private ReservationRepository reservationRepository;
    private ReservationMapper reservationMapper;
    private RoomMapper roomMapper;
    private DateProvider dateProvider;

    @Autowired
    public ReservationService(RoomRepository roomRepository, ReservationRepository reservationRepository,
                              ReservationMapper reservationMapper, RoomMapper roomMapper,
                              DateProvider dateProvider) {
        this.roomRepository = roomRepository;
        this.reservationRepository = reservationRepository;
        this.reservationMapper = reservationMapper;
        this.roomMapper = roomMapper;
        this.dateProvider = dateProvider;
    }

    @Transactional
    public ReservationStatusDTO roomReservation(Long id, LocalDate startDate, LocalDate endDate) {
        dateCheck(startDate, endDate);
        return createReservation(id, startDate, endDate);
    }

    private ReservationStatusDTO createReservation(Long id, LocalDate startDate, LocalDate endDate) {
        Room room = roomRepository.findOne(id);
        ReservationStatusDTO reservationStatusDTO = new ReservationStatusDTO();
        if(checkIfRoomAvailable(room, startDate, endDate)) {
            room.setReservations(room.getReservations());
            Reservation reservation = reservationMapper.reservationParamToEntity(startDate, endDate);
            List<Reservation> reservationList = room.getReservations();
            reservationList.add(reservation);
            saveEntitiesToDatabase(reservation, room);
            reservationStatusDTO.setStatus(ReservationStatus.SUCCESSFUL);
            reservationStatusDTO.setStatusDetails("Your booking accepted");
        } else {
            reservationStatusDTO.setStatus(ReservationStatus.FAILED);
            reservationStatusDTO.setStatusDetails("Room is not available for that period");
        }
        return reservationStatusDTO;
    }

    private boolean checkIfRoomAvailable(Room room, LocalDate startDate, LocalDate endDate) {
        List<Reservation> reservationList = room.getReservations();
        boolean isRoomAvailable = true;
        for (Reservation reservations : reservationList) {
            if (startDate.isBefore(reservations.getEndDate())
                    || endDate.isAfter(reservations.getStartDate())) {
                isRoomAvailable = false;
            }
        }
        return isRoomAvailable;
    }

    private void saveEntitiesToDatabase(Reservation reservation, Room room) {
        reservationRepository.save(reservation);
        roomRepository.save(room);
    }

    @Transactional
    public Iterable<RoomDTO> checkAvailability(LocalDate startDate, LocalDate endDate) {
        dateCheck(startDate, endDate);
        Iterable<Room> roomIterable = roomRepository.findAll();
        List<Room> roomList = new ArrayList<>();
        roomIterable.forEach(roomList::add);
        return findAvailableRoomsInDatabase(roomList,startDate, endDate);
    }

    private HashSet<RoomDTO> findAvailableRoomsInDatabase(List<Room> roomList, LocalDate startDate,
                                                          LocalDate endDate) {
        HashSet<RoomDTO> availableRoomList = new HashSet<>();
        for(Room viewRoom: roomList){
            List<Reservation> reservationList = viewRoom.getReservations();
            if(isRoomAvailable(reservationList,startDate,endDate)
            || viewRoom.getReservations().isEmpty()) {
                RoomDTO roomDTO = roomMapper.roomEntityToRoomDTO(viewRoom);
                availableRoomList.add(roomDTO);
            }
        }
        return availableRoomList;
    }

    private boolean isRoomAvailable(List<Reservation> reservationList, LocalDate startDate,
                                             LocalDate endDate){
        boolean isRoomAvailableForReservation = true;
        for (Reservation viewReservation : reservationList) {
            if (startDate.isBefore(viewReservation.getEndDate())
                    && viewReservation.getStartDate().isBefore(endDate)) {
                isRoomAvailableForReservation = false;
            }
        }
        return isRoomAvailableForReservation;
    }

    private void dateCheck(LocalDate startDate, LocalDate endDate) {
        if (startDate.isBefore(dateProvider.today()) || endDate.isBefore(startDate)) {
            throw new IllegalStateException();
        }
    }
}


