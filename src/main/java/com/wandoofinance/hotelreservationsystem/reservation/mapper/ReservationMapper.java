package com.wandoofinance.hotelreservationsystem.reservation.mapper;

import com.wandoofinance.hotelreservationsystem.reservation.entity.Reservation;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class ReservationMapper {
    public Reservation reservationParamToEntity(LocalDate startDate, LocalDate endDate){
        Reservation reservation = new Reservation();
        reservation.setStartDate(startDate);
        reservation.setEndDate(endDate);
        return reservation;
    }
}
