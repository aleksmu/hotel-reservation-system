package com.wandoofinance.hotelreservationsystem.reservation.repository;

import com.wandoofinance.hotelreservationsystem.reservation.entity.Reservation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long> {
}
