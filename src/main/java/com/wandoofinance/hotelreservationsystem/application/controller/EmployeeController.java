package com.wandoofinance.hotelreservationsystem.application.controller;

import com.wandoofinance.hotelreservationsystem.application.dto.StatisticsDTO;
import com.wandoofinance.hotelreservationsystem.room.dto.RoomDTO;
import com.wandoofinance.hotelreservationsystem.reservation.entity.Reservation;
import com.wandoofinance.hotelreservationsystem.room.entity.Room;
import com.wandoofinance.hotelreservationsystem.room.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;

@RestController
@RequestMapping("/admin/room")
public class EmployeeController {
    private RoomService roomService;

    @Autowired
    public EmployeeController(RoomService roomService) {
        this.roomService = roomService;
    }

    @GetMapping
    public Iterable<Room> showAllRooms() {
        return roomService.viewAllRooms();
    }

    @PostMapping
    public Room addRoomToDatabase(@RequestBody RoomDTO roomDTO) {
        return roomService.addRoomToDatabase(roomDTO);
    }

    @GetMapping("/{id}")
    public Room viewRoom(@PathVariable("id") Long id) {
        return roomService.viewRoom(id);
    }

    @PutMapping("/{id}")
    public Room editRoom(@PathVariable("id") Long id, @RequestBody RoomDTO roomDTO) {
        return roomService.editRoom(id, roomDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteRoom(@PathVariable("id") Long id) {
        roomService.deleteRoom(id);
    }

    @GetMapping("/{id}/schedule")
    public Iterable<Reservation> showScheduleForEachRoom(@PathVariable("id") Long id) {
        return roomService.showScheduleForRoom(id);
    }

    @GetMapping("/statistics")
    public StatisticsDTO showStatisticsForAllRooms(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                   @RequestParam("startDate") LocalDate startDate,
                                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                   @RequestParam("endDate") LocalDate endDate)
    {
        return roomService.showStatistics(startDate,endDate);
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(IllegalStateException.class)
    void handleIllegalState() {
    }

    @ResponseStatus(CONFLICT)
    @ExceptionHandler(IllegalArgumentException.class)
    void handleIllegalArgument() {
    }

}
