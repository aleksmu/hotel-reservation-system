package com.wandoofinance.hotelreservationsystem.application.controller;

import com.wandoofinance.hotelreservationsystem.reservation.dto.ReservationStatusDTO;
import com.wandoofinance.hotelreservationsystem.room.dto.RoomDTO;
import com.wandoofinance.hotelreservationsystem.reservation.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;

@RestController
@RequestMapping("/room")
public class CustomerController {
    private ReservationService reservationService;

    @Autowired
    public CustomerController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping("/availability")
    public Iterable<RoomDTO> checkAvailability(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                               @RequestParam("startDate") LocalDate startDate,
                                               @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                               @RequestParam("endDate") LocalDate endDate) {
        return reservationService.checkAvailability(startDate, endDate);
    }

    @PostMapping("/{id}/reservation")
    public ReservationStatusDTO makeRoomReservation(@PathVariable("id") Long id,
                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                    @RequestParam("startDate") LocalDate startDate,
                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                    @RequestParam("endDate") LocalDate endDate) {
        return reservationService.roomReservation(id, startDate, endDate);
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(IllegalStateException.class)
    void handleIllegalState() {
    }

    @ResponseStatus(CONFLICT)
    @ExceptionHandler(IllegalArgumentException.class)
    void handleIllegalArgument() {
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(NullPointerException.class)
    void handleNullPointerException() {
    }

}
