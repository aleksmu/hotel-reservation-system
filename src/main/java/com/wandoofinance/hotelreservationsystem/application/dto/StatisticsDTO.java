package com.wandoofinance.hotelreservationsystem.application.dto;

public class StatisticsDTO {

    private Integer availableRoomCount;
    private Integer occupiedRoomCount;

    public Integer getAvailableRoomCount() {
        return availableRoomCount;
    }

    public void setAvailableRoomCount(Integer availableRoomCount) {
        this.availableRoomCount = availableRoomCount;
    }

    public Integer getOccupiedRoomCount() {
        return occupiedRoomCount;
    }

    public void setOccupiedRoomCount(Integer occupiedRoomCount) {
        this.occupiedRoomCount = occupiedRoomCount;
    }
}
