package com.wandoofinance.hotelreservationsystem.application.service;

import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DateProvider {

    private LocalDate today = LocalDate.now();

    public void setCurrentDate(LocalDate today) {
        this.today = today;
    }

    public LocalDate today() {
        return this.today;
    }

}
